﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class QuestionsControl : MonoBehaviour {

	public GameObject[] questions;
	public Button[] rightAnswers;
	public GameObject button;
	public GameObject messageText;
	public GameObject exit;
	private int QuestionNumber = 0;
	private Color green = Color.green;
	private Color red = Color.red;
	private int rightCount = 0;
	void Start () {
		
	}

	public void Questions()
	{
		messageText.SetActive (false);
		questions [QuestionNumber].SetActive (true);
	}

	public void OnVariantClick()
	{
		Button[] buttons = questions[QuestionNumber].GetComponentsInChildren<Button> ();

		GameObject cur = EventSystem.current.currentSelectedGameObject;
		Button right = rightAnswers [QuestionNumber];
		Button temp = cur.GetComponent<Button> ();
		ColorBlock cb = temp.colors;
		cb.normalColor = red;
		cb.disabledColor = red;
		temp.colors = cb;

		temp = right.GetComponent<Button> ();
		cb = temp.colors;
		cb.normalColor = green;
		cb.disabledColor = green;
		temp.colors = cb;
		if (cur.GetComponent<Button> ().colors.normalColor == green)
			rightCount++;
		for (int i = 0; i < 3; i++)
			buttons [i].GetComponent<Button>().interactable = false;
		button.SetActive (true);
	}

	public void OnNextOrExitClick()
	{
		button.SetActive (false);
		if (QuestionNumber == questions.Length - 1) {
			questions [QuestionNumber].SetActive (false);
			exit.SetActive (true);
			Debug.Log (rightCount);
			exit.GetComponentInChildren<Text> ().text = "Ваш результат: " + rightCount.ToString () + "/" + questions.Length.ToString (); 
		} else {
			questions [QuestionNumber].SetActive (false);
			QuestionNumber++;
			questions [QuestionNumber].SetActive (true);
		}
	}
}
