﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ElectronsMovement : MonoBehaviour {

	private Rigidbody[] rbs;
	private Transform[] pos;
	private int frequency = 8;
	private Vector3 force = new Vector3();
	private System.Random rand = new System.Random();
	public static int k = 1;
	public static int maxSpeed = 15;
	private GameObject left, right;
	private Quaternion quat = new Quaternion(0, 0, 0, 0);
	private float collider = 0.1f;
	public static bool needUpdate = false;
	void Start () {
		rbs = GetComponentsInChildren<Rigidbody> ();
		pos = GetComponentsInChildren<Transform> ();
		left = GameObject.Find ("Left");
		right = GameObject.Find ("Right");
		InvokeRepeating ("AddForces", 0, 0.15f);
	}
	

	void Update () {
		k = (k + 1) % frequency;
		if (needUpdate) {
			needUpdate = false;
			AddForces ();
		}
	}
	private void AddForces()
	{
		if (TurnOnOff.cur == 0) {
			for (int i = 0; i < rbs.Length; i++) {
				force = new Vector3 (rand.Next (maxSpeed), rand.Next (maxSpeed), rand.Next (maxSpeed));
				//if (i == 0) Debug.Log (force);
				rbs [i].AddRelativeForce (force);
			}
		} else {
			force = new Vector3 (ChangePoles.side * maxSpeed / 4, 0, 0);
			for (int i = 0; i < rbs.Length; i++) {
				if (right.transform.position.x - collider - pos [i].position.x - 0.0001f <= pos [i].localScale.x && ChangePoles.side == 1) {
					//Debug.Log ((-left.transform.position.x) * 10);
					pos [i].SetPositionAndRotation (new Vector3 (left.transform.position.x + collider + pos [i].localScale.x, pos [i].position.y, pos [i].position.z), quat);
				} else 
				if (pos[i].position.x - left.transform.position.x - collider - 0.0001f <= pos [i].localScale.x && ChangePoles.side == -1) {
					//Debug.Log (right.transform.position.x * 10);
					pos [i].SetPositionAndRotation (new Vector3 (right.transform.position.x - pos [i].localScale.x - collider, pos [i].position.y, pos [i].position.z), quat);
				}
				rbs [i].AddForce (force);
			}
		}

			
	}
}
