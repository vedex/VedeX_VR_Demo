﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePoles : MonoBehaviour {

	private GameObject battery;
	public static int side = 1;
	public void Start()
	{
		battery = GameObject.Find ("Battery");
	}
	public void RotateBattery()
	{
		side *= -1;
		ElectronsMovement.needUpdate = true;
		battery.transform.Rotate (new Vector3 (180, 0, 0));
	}
}
