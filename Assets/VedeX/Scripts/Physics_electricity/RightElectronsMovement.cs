﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RightElectronsMovement : MonoBehaviour {

	private Rigidbody[] rbs;
	private Transform[] wires;
	private GameObject[] pos;
	private Transform wall;
	private readonly float MIN = 0.01f;
	private static float speed = 1;
	private Vector3 UP = new Vector3(0, speed, 0);
	private Vector3 DOWN = new Vector3(0, -speed, 0);
	private Vector3 LEFT = new Vector3(-speed, 0, 0);
	private Vector3 RIGHT = new Vector3(speed, 0, 0);
	private Vector3 batterySpawn, wallSpawn;
	void Start () {
		rbs = GetComponentsInChildren<Rigidbody> ();
		pos = GameObject.FindGameObjectsWithTag ("RightWireElectron");
		Debug.Log (pos.Length);
		wires = GameObject.Find ("RightWire").GetComponentsInChildren<Transform> ();
		wall = GameObject.Find ("Right").GetComponent<Transform> ();
		batterySpawn = new Vector3 (wires [3].position.x - wires [3].localScale.y + pos [1].transform.localScale.x + MIN, wires [3].position.y, wires [3].position.z);
		wallSpawn = new Vector3 (wires [1].position.x - wires [1].localScale.y + pos [1].transform.localScale.x + MIN, wires [1].position.y, wires [1].position.z);
		AddForces ();
	}


	void FixedUpdate () {
		if (TurnOnOff.cur == 1)
			AddForces ();
	}

	void AddForces() {

		for (int i = 0; i < rbs.Length; i++) {
			Rigidbody rb = pos [i].GetComponent<Rigidbody> ();
			Vector3 curPos = pos [i].transform.position;
			if (ChangePoles.side == -1) {
				if (wires [2].position.x - curPos.x > MIN && Math.Abs (curPos.y - wires [3].position.y) < MIN) {
					rb.velocity = RIGHT;
				} else if (curPos.y - wires [1].position.y > MIN && Math.Abs (curPos.x - wires [2].position.x) < MIN) {
					rb.velocity = DOWN;
				} else if (curPos.x - wall.position.x > MIN && Math.Abs (curPos.y - wires [1].position.y) < MIN) {
					rb.velocity = LEFT;
				} else {
					rb.MovePosition (batterySpawn);
					rb.velocity = RIGHT;
				}

			} else {
				if (wires [2].position.x - curPos.x > MIN && Math.Abs (curPos.y - wires [1].position.y) < MIN) {
					rb.velocity = RIGHT;
				} else if (wires [3].position.y - curPos.y > MIN && Math.Abs (curPos.x - wires [2].position.x) < MIN) {
					rb.velocity = UP;
				} else if (curPos.x - batterySpawn.x > MIN && Math.Abs (curPos.y - wires [3].position.y) < MIN) {
					rb.velocity = LEFT;
				} else {
					rb.MovePosition (wallSpawn);
					rb.velocity = RIGHT;
				}
			}
		}
	}

}
