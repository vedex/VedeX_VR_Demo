﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnOff : MonoBehaviour {

	public static GameObject wires;
	public GameObject player;
	private GameObject circuit;
	private GameObject begin;
	void Start () {
		wires = GameObject.Find ("Wires");
		//wires.SetActive (false);
		circuit = GameObject.Find ("MainObjects");
		circuit.SetActive (false);
		begin = GameObject.Find ("Begin");
	}
	

	public void OnClick () {
		player.transform.position = new Vector3 (0, 2.2f, -3);
		circuit.SetActive (true);
		wires.SetActive (false);
		begin.SetActive (false);
	}
}
