﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnOnOff : MonoBehaviour {
	private string[] headers = {"Замкнуть цепь", "Разомкнуть цепь"};
	public static int cur = 0;
	public Text text;
	public GameObject wires;
	public void Start () {
		text.text = headers [cur];
	}

	public void TaskOnClick () {
		cur = (cur + 1) % 2;
		ElectronsMovement.needUpdate = true;
		text.text = headers [cur];
		//wires.SetActive (true);
		wires.SetActive (!wires.activeSelf);

	}
}
