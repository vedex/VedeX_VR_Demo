﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestMenuScript : MonoBehaviour {

//	public objects

	[SerializeField]
	public Canvas coordinateTable,
				  questionCanvas,
				  menuCanvas,
				  helpFuncCanvas;
	
	public Text helloText,
				lvl_1,
				lvl_1_2,
				lvl_2,
				contTaskText,
				lvl_4;

	public GameObject graph,
					  btr,
					  cube,
					  lvl_2Panel;

	public Button loadStep_2;
					  

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void loadHello(){
		onEndLvl_4();
		helloText.gameObject.SetActive(true);
	}
	public void onStartLessonClicked(){
		coordinateTable.gameObject.SetActive(true);
		helpFuncCanvas.gameObject.SetActive(true);
		helloText.gameObject.SetActive(false);
		lvl_1.gameObject.SetActive(true);
	}
	public void onEndLessonClicked(){
		helloText.gameObject.SetActive(false);
		lvl_4.gameObject.SetActive(true);
		
	}
	public void onLoadStep_2(){
		onEndStartLesson();
		lvl_2.gameObject.SetActive(true);
		cube.SetActive(true);
		btr.SetActive(true);
		lvl_2Panel.SetActive(true);
	}
	
	public void onLoadContrTask(){
		onEndStep_2();
		onEndLvl_4();
		contTaskText.gameObject.SetActive(true);
	}

	public void onLoadContrTaskClicked(){
		menuCanvas.gameObject.SetActive(false);
		questionCanvas.gameObject.SetActive(true);
	}
	public void onLoadLvl_4(){
		helloText.gameObject.SetActive(false);
		lvl_4.gameObject.SetActive(true);
	}


	public void onEndStartLesson(){
		lvl_1_2.gameObject.SetActive(false);
		helpFuncCanvas.gameObject.SetActive(false);
		graph.SetActive(false);
		coordinateTable.gameObject.SetActive(false);
	}

	public void onEndStep_2(){
		lvl_2.gameObject.SetActive(false);
		cube.SetActive(false);
		btr.SetActive(false);
		lvl_2Panel.SetActive(false);
	}
	public void onEndLvl_4(){
		lvl_4.gameObject.SetActive(false);
	}
}
