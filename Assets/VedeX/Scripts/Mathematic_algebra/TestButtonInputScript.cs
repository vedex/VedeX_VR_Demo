﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestButtonInputScript : MonoBehaviour {

	// Use this for initialization

	public static string coordinate;
	public Text txt;
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(txt != null)
			txt.text = coordinate;
	}

	public void onZeroClicked(){
		coordinate += "0";
	}
	public void onOneClicked(){
		coordinate += "1";
	}
	public void onTwoClicked(){
		coordinate += "2";
	}
	public void onThreeClicked(){
		coordinate += "3";
	}
	public void onFourClicked(){
		coordinate += "4";
	}
	public void onFiveClicked(){
		coordinate += "5";
	}
	public void onSixClicked(){
		coordinate += "6";
	}
	public void onSevenClicked(){
		coordinate += "7";
	}
	public void onEightClicked(){
		coordinate += "8";
	}
	public void onNineClicked(){
		coordinate += "9";
	}

	public void onDeleteClicked(){
		if(coordinate.Length != 0)
			coordinate = coordinate.Remove(coordinate.Length-1);
	}
	
}
