﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestTableScript : TestButtonInputScript {
//	public object
	[SerializeField]
	public Text coordinateText_0_1,
				coordinateText_0_3,
				coordinateText_1_2,
				coordinateText_1_4;	
	public Image rightOrNoCanvas_0_1,
				 rightOrNoCanvas_0_3,
				 rightOrNoCanvas_1_2,
				 rightOrNoCanvas_1_4;
	public Canvas numberCanvas;

	public GameObject graph;

	public Button next;

	public Text lvl_1,
				lvl_1_2;
				



	private int counter = 0;  
					  
	// Use this for initialization
	void Start () {
		next.enabled = false;
		UpdateColor(rightOrNoCanvas_0_1);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void onTextClicked(){
		numberCanvas.gameObject.SetActive(true);
	}

	private void UpdateColor(Image image){
		image.color = Color.gray;
	}

	private void checkAnswer(Text text, Image image, Image next,string answer){
			if(coordinate == answer){
					text.text = coordinate;
					image.color = Color.green;
					coordinate = "";
					if(next != null)
						UpdateColor(next);
					counter++;
				}
				else{
					text.text = "ERROR";
					image.color = Color.red;
					coordinate = null;
				}
	}

	public void onOkClicked(){
		switch(counter){
			case 0:
				checkAnswer(coordinateText_0_1, rightOrNoCanvas_0_1, rightOrNoCanvas_0_3, Answers.linearAnswer[0]);
				break;
				
			case 1:
				checkAnswer(coordinateText_0_3, rightOrNoCanvas_0_3, rightOrNoCanvas_1_2, Answers.linearAnswer[1]);
				break;
			case 2:
				checkAnswer(coordinateText_1_2, rightOrNoCanvas_1_2, rightOrNoCanvas_1_4, Answers.linearAnswer[2]);
				break;
			case 3:
				checkAnswer(coordinateText_1_4, rightOrNoCanvas_1_4, null, Answers.linearAnswer[3]);
				if(counter == 4){
					numberCanvas.gameObject.SetActive(false);
					lvl_1.gameObject.SetActive(false);
					lvl_1_2.gameObject.SetActive(true);
					graph.SetActive(true);
					next.enabled = true;break;
				}break;
		}
		
	}
}
