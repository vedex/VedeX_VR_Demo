﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGraphController : MonoBehaviour {

	private LineRenderer lRender;

	// Use this for initialization
	void Start () {
		lRender = GetComponent<LineRenderer>();

		Vector3[] positions = new Vector3[5];


		positions[0] = new Vector3(-0.5f, -0.4f, 0.0f);
		positions[1] = new Vector3(-0.3f, -0.2f, 0.0f);
		positions[2] = new Vector3(0.0f, 0.1f, 0.0f);
		positions[3] = new Vector3(0.2f, 0.3f, 0.0f);
		positions[4] = new Vector3(0.4f, 0.5f, 0.0f);

		lRender.positionCount = positions.Length;
		lRender.SetPositions(positions);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
