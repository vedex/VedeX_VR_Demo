﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCarScript : MonoBehaviour {

	public GameObject player;
	public static float speed = 0.05f,
						constSpeed = 0.05f;

	public static float pos;

	public float pos_y = 0;

	// Use this for initialization
	void Start () {
		pos  = player.transform.position.x;
		pos_y = player.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if(pos > 3.9f){
			pos = -2.31f;
			pos_y = 1.35f;
			speed = constSpeed;
		}
		speed += 0.17f/1000f;
		pos+=speed;
		pos_y += -speed / 5.2f;
		player.transform.position = new Vector3(pos, pos_y, player.transform.position.z);
	}
}
