﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;

public class TestLevel3Controller : MonoBehaviour {

	// Use this for initialization
	[SerializeField]
	public Text  speed_1,
				 path_1;
	public Slider slider;


	void Start () {
		if(slider != null){
			slider.maxValue = 20;
			slider.minValue = 1;
			slider.value = 5;
		}
		slider.onValueChanged.AddListener(delegate {onValueChanged();});
	}
	
	// Update is called once per frame
	void Update () {
		speed_1.text = (TestCarScript.speed*100).ToString();
		path_1.text = TestCarScript.pos.ToString();
	}

	private void onValueChanged(){
		TestCarScript.speed = slider.value/100;
		TestCarScript.constSpeed = slider.value/100;
	}
}
