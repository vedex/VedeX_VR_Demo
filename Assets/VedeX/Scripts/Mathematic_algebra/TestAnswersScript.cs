﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestAnswersScript : MonoBehaviour {


	public Text q_1,
				q_2,
				q_3,
				win,
				lose;
	public Toggle dp_1_1,
					dp_1_2,
					dp_1_3,
					dp_2_1,
					dp_2_2,
					dp_2_3,
					dp_3_1,
					dp_3_2,
					dp_3_3;

	//right answers
	//user answers
	private List<int> listOfAnswers = new List<int>{};

	private int counter = 0;
	private bool correct = false;

	// Use this for initialization
	void Start () {
		//check how use c++ list = {...};
		// _listOfAnswers[0] = 2;
		// _listOfAnswers[1] = 2;
		// _listOfAnswers[2] = 3;

		//add delegate
		dp_1_1.onValueChanged.AddListener(delegate {onDp_1_1Changed();});
		dp_1_2.onValueChanged.AddListener(delegate {onDp_1_2Changed();});
		dp_1_3.onValueChanged.AddListener(delegate {onDp_1_3Changed();});
		dp_2_1.onValueChanged.AddListener(delegate {onDp_2_1Changed();});
		dp_2_2.onValueChanged.AddListener(delegate {onDp_2_2Changed();});
		dp_2_3.onValueChanged.AddListener(delegate {onDp_2_3Changed();});
		dp_3_1.onValueChanged.AddListener(delegate {onDp_3_1Changed();});
		dp_3_2.onValueChanged.AddListener(delegate {onDp_3_2Changed();});
		dp_3_3.onValueChanged.AddListener(delegate {onDp_3_3Changed();});
	}
	
	// Update is called once per frame
	void Update () {
	}

	//1
	public void onDp_1_1Changed(){
		listOfAnswers.Add(1);
		q_1.gameObject.SetActive(false);
		q_2.gameObject.SetActive(true);
	}
	public void onDp_1_2Changed(){
		listOfAnswers.Add(2);
		q_1.gameObject.SetActive(false);
		q_2.gameObject.SetActive(true);
	}
	public void onDp_1_3Changed(){
		listOfAnswers.Add(3);
		q_1.gameObject.SetActive(false);
		q_2.gameObject.SetActive(true);
	}
	//2
	public void onDp_2_1Changed(){
		listOfAnswers.Add(1);
		q_2.gameObject.SetActive(false);
		q_3.gameObject.SetActive(true);
	}
	public void onDp_2_2Changed(){
		listOfAnswers.Add(2);
		q_2.gameObject.SetActive(false);
		q_3.gameObject.SetActive(true);
	}
	public void onDp_2_3Changed(){
		listOfAnswers.Add(3);
		q_2.gameObject.SetActive(false);
		q_3.gameObject.SetActive(true);
	}
	//3
	public void onDp_3_1Changed(){
		listOfAnswers.Add(1);
		q_3.gameObject.SetActive(false);
		checkAnswers();
	}
	public void onDp_3_2Changed(){
		listOfAnswers.Add(2);
		q_3.gameObject.SetActive(false);
		checkAnswers();
	}
	public void onDp_3_3Changed(){
		listOfAnswers.Add(3);
		q_3.gameObject.SetActive(false);
		checkAnswers();
	}
	public void checkAnswers(){
		for(int i = 0; i < Answers._listOfCounterAnswers.Count; i++)
			if(Answers._listOfCounterAnswers[i] == listOfAnswers[i])
				counter++;
			
		if(counter == 3)
			win.gameObject.SetActive(true);
		else
			lose.gameObject.SetActive(true);
		
	}
}
