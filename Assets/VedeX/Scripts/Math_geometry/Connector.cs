﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Connector : MonoBehaviour
{
    private const int PRISM = 0;
    private const int PYRAMID = 1;
    private const float ACCURACY_LENGTH = 5e-1f ;
    private const float ACCURACY_ANGLE = 15f ;
    private string[] taskText =
    {
        "Добро пожаловать в виртуальный урок! В данном задании вам необходимо построить из данной фигуры правильную ",
        ". Для построения фигуры перемещайте её точки:\n1. Наведите прицел на вершину и нажмите на кнопку для её захвата\n" +
            "2. Переместите прицел на желаемую позицию точки и отпустите кнопку для её перемещения"
    };
    private const string PRISM_NAME = "призму";
    private const string PYRAMID_NAME = "пирамиду";

    //private LineRenderer lineRenderer;
    //private LineRenderer lineRendererCorrect;
    //private LineRenderer lr;
    private int numOfDots;
    public GameObject dot;
    public Text startText;
    public GameObject messagePanel;
    public Button button;
    private List<GameObject> dots;
    private List<Vector3> positions;
    private int figureType = 0;
    private bool figureBuilt = false;
    List<List<short>> edges;
    List<List<GameObject>> lineRendersObjects;
    private GameObject sampleRendererObject;
    private List<GameObject> sampleDots;
    private List<Vector3> samplePositions;

    private int numOfedges()
    {
        switch (figureType)
        {
            case PRISM:
                return 3 * numOfDots / 2;
            case PYRAMID:
                return 2 * (numOfDots - 1);
            default:
                return 0;
        }
    }

    void initPrismListOfedges()
    {
        for (int i = 0; i < numOfDots; i++)
        {
            edges.Add(new List<short>(numOfDots));
            for (int j = 0; j < numOfDots; j++) edges[i].Add(0);
        }
    }

    void initPyramidListOfedges()
    {
        for (int i = 0; i < numOfDots; i++)
        {
            edges.Add(new List<short>(numOfDots));
            for (int j = 0; j < numOfDots; j++) edges[i].Add(0);
        }
    }

    void initListOfedges()
    {
        edges = new List<List<short>>(numOfDots);
        switch (figureType)
        {
            case PRISM:
                initPrismListOfedges();
                break;
            case PYRAMID:
                initPyramidListOfedges();
                break;
            default:
                break;
        }
    }

    void initGameObjects()
    {
        lineRendersObjects = new List<List<GameObject>>(numOfDots);
        for(int i = 0; i < numOfDots; i++)
        {
            lineRendersObjects.Add(new List<GameObject>(numOfDots));
            for(int j = 0; j < numOfDots; j++)
            {
                lineRendersObjects[i].Add(new GameObject());
            }
        }
        sampleRendererObject = new GameObject();
    }

    // Use this for initialization
    void Start()
    {
        dots = new List<GameObject>();
        sampleDots = new List<GameObject>();
        positions = new List<Vector3>();
        samplePositions = new List<Vector3>();
        createFigure();
        createTextTask();
        initListOfedges();
        initGameObjects();
        renderLine();
        renderSample();
        button.GetComponentsInChildren<Text>()[0].text = "Выйти из урока";
    }

    void renderSamplePrism(int samplePulledDot)
    {
        if (samplePulledDot == -1)
        {
            sampleDots.Add(Instantiate(dot, new Vector3(1, 2, 1), Quaternion.identity));
            sampleDots.Add(Instantiate(dot, new Vector3(1, 1, 1), Quaternion.identity));
            sampleDots.Add(Instantiate(dot, new Vector3(2, 1, 1), Quaternion.identity));
            sampleDots.Add(Instantiate(dot, new Vector3(2, 2, 1), Quaternion.identity));
            //
            sampleDots.Add(Instantiate(dot, new Vector3(1, 2, 2), Quaternion.identity));
            sampleDots.Add(Instantiate(dot, new Vector3(1, 1, 2), Quaternion.identity));
            sampleDots.Add(Instantiate(dot, new Vector3(2, 1, 2), Quaternion.identity));
            sampleDots.Add(Instantiate(dot, new Vector3(2, 2, 2), Quaternion.identity));
        }
        else
        {
            var pulledDotOffset = sampleDots[samplePulledDot].transform.position - samplePositions[samplePulledDot];
            for (int i = 0; i < numOfDots; i++)
            {
                if (i == samplePulledDot) continue;
                sampleDots[i].transform.position += pulledDotOffset;
            }
        }

        LineRenderer lr;
        lr = sampleRendererObject.GetComponent<LineRenderer>();
        if (lr == null)
        {
            lr = sampleRendererObject.AddComponent<LineRenderer>() as LineRenderer;
        }
        lr.SetWidth(.1f, .1f);
        lr.positionCount = 5 * 6;
        lr.material.color = Color.green;
        // first render base
        int ind = renderSampeBase(0, 4, lr, 0);
        ind = renderSampeBase(4, 8, lr, ind);
        ind = renderSampeBase(new int[] { 0, 4, 7, 3}, lr, ind);
        ind = renderSampeBase(new int[] { 1, 5, 4, 0 }, lr, ind);
        ind = renderSampeBase(new int[] { 2, 6, 5, 1 }, lr, ind);
        ind = renderSampeBase(new int[] { 3, 7, 6, 2 }, lr, ind);
    }


    void renderSamplePyramid(int samplePulledDot)
    {
        //You miss every shot you don't take
        //for (int i = 0; i < 5; i++ ) //numOfDots - 1
        if(samplePulledDot == -1)
        {
            sampleDots.Add(Instantiate(dot, new Vector3(1, 2, 1), Quaternion.identity));
            sampleDots.Add(Instantiate(dot, new Vector3(1, 2, 2), Quaternion.identity));
            sampleDots.Add(Instantiate(dot, new Vector3(2, 2, 2), Quaternion.identity));
            sampleDots.Add(Instantiate(dot, new Vector3(2, 2, 1), Quaternion.identity));
            //
            sampleDots.Add(Instantiate(dot, new Vector3(1.5f, 2.5f, 1.5f), Quaternion.identity));
        }
        else
        {
            var pulledDotOffset = sampleDots[samplePulledDot].transform.position - samplePositions[samplePulledDot];
            for (int i = 0; i < numOfDots; i++)
            {
                if (i == samplePulledDot) continue;
                sampleDots[i].transform.position += pulledDotOffset;
            }
        }

        LineRenderer lr;
        lr = sampleRendererObject.GetComponent<LineRenderer>();
        if (lr == null)
        {
            lr = sampleRendererObject.AddComponent<LineRenderer>() as LineRenderer;
        }
        lr.SetWidth(.1f, .1f);
        lr.positionCount = numOfDots + 2 * (numOfDots - 1) + 1;
        lr.material.color = Color.green;
        // first render base
        int ind = renderSampeBase(0, numOfDots - 1, lr, 0);
        // render edges
        lr.SetPosition(ind++, sampleDots[numOfDots - 1].transform.position);
        for(int i = 0; i < numOfDots - 1; i++)
        {
            lr.SetPosition(ind++, sampleDots[i].transform.position);
            lr.SetPosition(ind++, sampleDots[numOfDots - 1].transform.position);
        }
    }

    int renderSampeBase(int firstBaseDot, int lastBaseDot, LineRenderer lr, int posInd)
    {
        for(int i = firstBaseDot; i < lastBaseDot; i++)
        {
            lr.SetPosition(posInd++, sampleDots[i].transform.position);
        }
        lr.SetPosition(posInd++, sampleDots[firstBaseDot].transform.position);
        return posInd;
    }

    int renderSampeBase(int[] dotsIndexes, LineRenderer lr, int posInd)
    {
        foreach (var index in dotsIndexes)
        {
            lr.SetPosition(posInd++, sampleDots[index].transform.position);
        }
        lr.SetPosition(posInd++, sampleDots[dotsIndexes[0]].transform.position);
        return posInd;
    }

    void renderSample(int samplePulledDot = -1)
    {
        switch (figureType)
        {
            case PRISM:
                renderSamplePrism(samplePulledDot);
                break;
            case PYRAMID:
                renderSamplePyramid(samplePulledDot);
                break;
            default:
                break;
        }
        //foreach (var dot in sampleDots) dot.name = "1";
        if(samplePulledDot == -1)
        {
            foreach (var dot in sampleDots)
            {
                dot.name = "2";
                samplePositions.Add(dot.transform.position);
            }
        }
        else
        {
            for(int i = 0; i < numOfDots; i++)
            {
                samplePositions[i] = sampleDots[i].transform.position;
            }
        }
    }

    void createTestPrism()
    {
        positions.Add(new Vector3(1, 1, 1));
        positions.Add(new Vector3(1, 0, 1));
        positions.Add(new Vector3(2, 0, 1));
        positions.Add(new Vector3(2, 1, 1));
        //
        positions.Add(new Vector3(1, 1, 2));
        positions.Add(new Vector3(1, 0, 2));
        positions.Add(new Vector3(2, 0, 2));
        positions.Add(new Vector3(2, 1, 2));
    }

    void createTestPyramid()
    {
        positions.Add(new Vector3(1, 1, 1));
        positions.Add(new Vector3(1, 0, 1));
        positions.Add(new Vector3(2, 0, 1));
        positions.Add(new Vector3(2, 1, 1));
        //
        positions.Add(new Vector3(1.5f, 0.5f, 2));
    }

    void createTextTask()
    {
        string figureName = "";
        if (figureType == PRISM)
        {
            figureName = PRISM_NAME;
        }
        else if (figureType == PYRAMID)
        {
            figureName = PYRAMID_NAME;
        }
        startText.text = taskText[0] + figureName + taskText[1];
    }

    void createPrism()
    {
        numOfDots = 8;
        figureType = PRISM;
        //createTestPrism();
        for (int i = 0; i < numOfDots; i++)
        {
            dots.Add(Instantiate(dot, new Vector3(Random.Range(-2f, 4f),
                Random.Range(0.5f, 4f), Random.Range(-2f, 4f)), Quaternion.identity));
            //dots.Add(Instantiate(dot, positions[i], Quaternion.identity));
            positions.Add(dots[i].transform.position);
        }
    }

    void createFigure()
    {
        //createPyramid();
        //createPrism();
        if (Random.Range(0, 10) % 2 == PRISM) createPrism();
        else createPyramid();
    }

    Color getEdgeColor(short edgeFlag)
    {
        switch (edgeFlag)
        {
            case 1:
                return Color.blue;
            case 2:
                return Color.green;
            default:
                return Color.yellow;
        }
    }

    private void renderPyramidFace(int i)
    {
        LineRenderer lr;
        int[] indexes = new int[] {i, (i + 1) % (numOfDots - 1), numOfDots - 1 , i};
        for(int j = 0; j < indexes.Length; j++)
        {
            int a = indexes[j], b = indexes[(j + 1) % indexes.Length];
            lr = lineRendersObjects[a][b].GetComponent<LineRenderer>();
            if (lr == null)
            {
                lr = lineRendersObjects[a][b].AddComponent<LineRenderer>() as LineRenderer;
                lr.SetWidth(.1f, .1f);
                lr.positionCount = 2;
            }
            lr.material.color = getEdgeColor(edges[a][b]);
            lr.SetPosition(0, dots[a].transform.position);
            lr.SetPosition(1, dots[b].transform.position);
        }
    }

    private void renderPrismFace(int i)
    {
        LineRenderer lr;
        int[] indexes = new int[] { i, (i + 1) % (numOfDots / 2), (i + 1) % (numOfDots / 2) + numOfDots / 2, i + numOfDots / 2, i };
        for (int j = 0; j < indexes.Length; j++)
        {
            int a = indexes[j], b = indexes[(j + 1) % indexes.Length];
            lr = lineRendersObjects[a][b].GetComponent<LineRenderer>();
            if (lr == null)
            {
                lr = lineRendersObjects[a][b].AddComponent<LineRenderer>() as LineRenderer;
                lr.SetWidth(.1f, .1f);
                lr.positionCount = 2;
            }
            lr.material.color = getEdgeColor(edges[a][b]);
            lr.SetPosition(0, dots[a].transform.position);
            lr.SetPosition(1, dots[b].transform.position);
        }
    }

    private void renderPrismBase(int firstPoint, int numOfPoints)
    {
        LineRenderer lr;

        int[] indexes = new int[numOfPoints + 1];
        for (int j = firstPoint; j < firstPoint + numOfPoints; j++) indexes[j - firstPoint] = j;
        indexes[numOfPoints] = firstPoint;
        for (int j = 0; j < indexes.Length; j++)
        {
            int a = indexes[j], b = indexes[(j + 1) % indexes.Length];
            lr = lineRendersObjects[a][b].GetComponent<LineRenderer>();
            if (lr == null)
            {
                lr = lineRendersObjects[a][b].AddComponent<LineRenderer>() as LineRenderer;
                lr.SetWidth(.1f, .1f);
                lr.positionCount = 2;
            }
            lr.material.color = getEdgeColor(edges[a][b]);
            lr.SetPosition(0, dots[a].transform.position);
            lr.SetPosition(1, dots[b].transform.position);
        }
    }

    private void renderPyramidBase()
    {
        LineRenderer lr;
        int[] indexes = new int[numOfDots];
        for (int j = 0; j < numOfDots - 1; j++) indexes[j] = j;
        for (int j = 0; j < indexes.Length; j++)
        {
            int a = indexes[j], b = indexes[(j + 1) % indexes.Length];
            lr = lineRendersObjects[a][b].GetComponent<LineRenderer>();
            if (lr == null)
            {
                lr = lineRendersObjects[a][b].AddComponent<LineRenderer>() as LineRenderer;
                lr.SetWidth(.1f, .1f);
                lr.positionCount = 2;
            }
            lr.material.color = getEdgeColor(edges[a][b]);
            lr.SetPosition(0, dots[a].transform.position);
            lr.SetPosition(1, dots[b].transform.position);
        }
    }

    private void renderPyramidFaces()
    {
        for (int i = 0; i < numOfDots - 1; i++)
        {
            renderPyramidFace(i);
        }
    }

    private void renderPrismFaces()
    {
        for (int i = 0; i < numOfDots / 2; i++)
        {
            renderPrismFace(i);
        }
    }

    private void renderLine()
    {
        if (figureType == PRISM)
        {
            renderPrismBase(0, numOfDots / 2);
            renderPrismBase(numOfDots / 2, numOfDots / 2);
            renderPrismFaces();
        }
        else if (figureType == PYRAMID)
        {
            renderPyramidFaces();
            renderPyramidBase();
        }
    }

    void createPyramid()
    {
        numOfDots = 5;
        figureType = PYRAMID;
        //createTestPyramid();
        for (int i = 0; i < numOfDots; i++)
        {
            dots.Add(Instantiate(dot, new Vector3(Random.Range(-2f, 4f),
                Random.Range(0.5f, 4f), Random.Range(-2f, 4f)), Quaternion.identity));
            //dots.Add(Instantiate(dot, positions[i], Quaternion.identity));
            positions.Add(dots[i].transform.position);
        }
    }

    void fixDots(params int[] indexes)
    {
        foreach(var index in indexes)
        {
            //dots[index].name = "1";
        }
    }

    void markEdges(bool flag, params int[] indexes)
    {
        for(int i = 0; i < indexes.Length; i++)
        {
            if(flag)
                edges[indexes[i]][indexes[(i + 1) % indexes.Length]] =
                    ++edges[indexes[(i + 1) % indexes.Length]][indexes[i]];
        }
    }
    
    bool validatePyramidEdges()
    {
        bool res = true;
        for (int i = 0; i < numOfDots - 1; i++)
        {
            var prevedgesDotInd = (i - 1 + numOfDots - 1) % (numOfDots - 1);
            float leftedges = (positions[numOfDots - 1] - positions[i]).magnitude;
            float rightedges = (positions[numOfDots - 1] - positions[prevedgesDotInd]).magnitude;
            if (approximatelyEqualLength(leftedges, rightedges))
            {
                markEdges(true, prevedgesDotInd, i, numOfDots - 1);
                //fixDots(numOfDots - 1, i, prevedgesDotInd);
            }
            else
            {
                markEdges(false, prevedgesDotInd, i, numOfDots - 1);
                res = false;
            }
        }
        return res;
    }

    bool validatePyramid()
    {
        var areEdgesOk = validatePyramidEdges();
        var isBaseOk = validateBase(numOfDots - 1, 360 / (numOfDots - 1));
        return areEdgesOk && isBaseOk;
    }

    bool approximatelyEqualLength(float a, float b)
    {
        return Mathf.Abs(a - b) <= ACCURACY_LENGTH;
    }

    bool approximatelyEqualAngle(float a, float b)
    {
        return Mathf.Abs(a - b) <= ACCURACY_ANGLE;
    }

    float angleBetweenVectors(Vector3 a, Vector3 b, Vector3 c)
    {
        Vector3 v1 = a - b;
        Vector3 v2 = c - b;
        return Mathf.Acos(Vector3.Dot(v1.normalized, v2.normalized)) * 180 / Mathf.PI;
    }

    bool validateBase(int lastBaseDot, float baseAngle, int firstPoint = 0)
    {
        bool res = approximatelyEqualAngle(baseAngle, angleBetweenVectors(positions[lastBaseDot - 1],
            positions[firstPoint], positions[firstPoint + 1]));
        for (int i = firstPoint + 1; i < lastBaseDot - 1; i++)
        {
            res &= approximatelyEqualAngle(baseAngle, angleBetweenVectors(positions[i - 1], positions[i],
                positions[i + 1]));
        }
        int[] dotsIndexes = new int[lastBaseDot - firstPoint];
        for (int i = firstPoint; i < lastBaseDot; i++) dotsIndexes[i - firstPoint] = i;
        markEdges(res, dotsIndexes);
        return res;
    }

    bool validateRectangle(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
    {
        return approximatelyEqualAngle(90f, angleBetweenVectors(d, a, b)) &&
            approximatelyEqualAngle(90f, angleBetweenVectors(a, b, c)) &&
            approximatelyEqualAngle(90f, angleBetweenVectors(b, c, d)) &&
            approximatelyEqualAngle(90f, angleBetweenVectors(c, d, a));
    }

    bool validatePrismFaces()
    {
        int half = numOfDots / 2;
        int lastBaseDot = half;
        bool res = true;
        if (!validateRectangle(positions[lastBaseDot - 1], positions[numOfDots - 1],
            positions[lastBaseDot], positions[0])) res = false;
        markEdges(res, lastBaseDot - 1, numOfDots - 1, lastBaseDot, 0);
        for (int i = 0; i < lastBaseDot - 1; i++)
        {
            if(validateRectangle(positions[i], positions[i + half], positions[i + 1 + half], positions[i + 1]))
            {
                markEdges(true, i, i + half, i + 1 + half, i + 1);
            }
            else
            {
                res = false;
                markEdges(false, i, i + half, i + 1 + half, i + 1);
            }
        }
        return res;
    }

    bool validatePrism()
    {
        bool[] areBasesOk = new bool[]{ validateBase(numOfDots / 2, 360 / (numOfDots / 2), 0),
        validateBase(numOfDots, 360 / (numOfDots / 2), numOfDots / 2) };
        bool areFacesOk = validatePrismFaces();
        return areBasesOk[0] && areBasesOk[1] && areFacesOk;
    }

    void clearEdgesMarks()
    {
        for(int i = 0; i < edges.Count; i++)
        {
            for (int j = 0; j < edges[i].Count; j++)
                edges[i][j] = edges[j][i] = 0;
        }
    }

    bool validateFigure()
    {
        clearEdgesMarks();
        if (figureType == PRISM)
            return validatePrism();
        return validatePyramid();
    }

    // Update is called once per frame
    void Update()
    {
        if (figureBuilt) return;
        bool was_rendered = false;
        bool was_rendered_sample = false;
        int samplePulledDot = -1;
        for (int i = 0; i < numOfDots; i++)
        {
            if (!dots[i].transform.position.Equals(positions[i]))
            {
                if (!was_rendered)
                {
                    //renderLine();
                    was_rendered = true;
                }
                positions[i] = dots[i].transform.position;
            }
            //
            if (!sampleDots[i].transform.position.Equals(samplePositions[i]))
            {
                samplePulledDot = i;
                //samplePositions[i] = sampleDots[i].transform.position;
            }
        }
        if (was_rendered)
        {
            if (validateFigure())
            {
                Debug.Log("VALIDATED");
                foreach(var dot in dots) dot.name = "1";
                foreach (var dot in sampleDots) dot.name = "1";
                startText.text = "Фигура построена!";
                button.gameObject.SetActive(true);
                //messagePanel.GetComponent<Text>[0].text
                figureBuilt = true;
            }
            renderLine();
        }
        if (samplePulledDot != -1)
        {
            renderSample(samplePulledDot);
        }
    }
}
