﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CustomEventTrigger : EventTrigger {

	// Use this for initialization
	void Start () {
		
	}

    //public override void OnPointerClick(PointerEventData data)
    //{
    //    Debug.Log("OnDrag called.");
    //}

    //public override void OnDrag(PointerEventData data)
    //{
    //    transform.position = data.position;
    //    Debug.Log("OnDrag called.");
    //}
    //public override void OnBeginDrag(PointerEventData data)
    //{
    //    Debug.Log("OnBeginDrag called.");
    //}

    //public override void OnEndDrag(PointerEventData data)
    //{
    //    transform.position = new Vector3(data.position.x, data.position.y, 2);
    //    Debug.Log("OnEndDrag called.");
    //}

    //public override void OnBeginDrag(PointerEventData data)
    //{
    //    Debug.Log("OnBeginDrag called.");
    //}

    //public override void OnCancel(BaseEventData data)
    //{
    //    Debug.Log("OnCancel called.");
    //}

    //public override void OnDeselect(BaseEventData data)
    //{
    //    Debug.Log("OnDeselect called.");
    //}

    //public override void OnDrag(PointerEventData data)
    //{
    //    Debug.Log("OnDrag called.");
    //}

    //public override void OnDrop(PointerEventData data)
    //{
    //    Debug.Log("OnDrop called.");
    //}

    //public override void OnEndDrag(PointerEventData data)
    //{
    //    Debug.Log("OnEndDrag called.");
    //}

    //public override void OnInitializePotentialDrag(PointerEventData data)
    //{
    //    Debug.Log("OnInitializePotentialDrag called.");
    //}

    //public override void OnMove(AxisEventData data)
    //{
    //    Debug.Log("OnMove called.");
    //}

    //public override void OnPointerClick(PointerEventData data)
    //{
    //    Debug.Log("OnPointerClick called.");
    //}

    //public override void OnPointerDown(PointerEventData data)
    //{
    //    Debug.Log("OnPointerDown called.");
    //    Debug.Log(data.position);
    //}

    //public override void OnPointerEnter(PointerEventData data)
    //{
    //    Debug.Log("OnPointerEnter called.");
    //}

    //public override void OnPointerExit(PointerEventData data)
    //{
    //    Debug.Log("OnPointerExit called.");
    //}
    bool dotInCube(Vector3 dotPos)
    {
        return dotPos.y >= this.transform.localScale.y &&
            dotPos.y <= 5 - this.transform.localScale.y &&
            dotPos.x >= -5 + this.transform.localScale.x &&
            dotPos.x <= 5 - this.transform.localScale.x &&
            dotPos.z >= -5 + this.transform.localScale.z &&
            dotPos.z <= 5 - this.transform.localScale.z;
    }
    float checkYAxis(Vector3 dotPos)
    {
        if (dotPos.y >= this.transform.localScale.y &&
            dotPos.y <= 5 - this.transform.localScale.y) return -100;
        return Mathf.Abs(dotPos.y - this.transform.localScale.y) <= 
            Mathf.Abs(5 - dotPos.y - transform.localScale.y) ? transform.localScale.y :
            5 - this.transform.localScale.y;
    }
    float checkXAxis(Vector3 dotPos)
    {
        if (dotPos.x >= -5 + this.transform.localScale.x &&
            dotPos.x <= 5 - this.transform.localScale.x) return -100;
        return Mathf.Abs(dotPos.x - (-5 + transform.localScale.x)) <= 
            Mathf.Abs(5 - dotPos.x - transform.localScale.x) ? -5 + transform.localScale.x : 
            5 - this.transform.localScale.x;
    }
    float checkZAxis(Vector3 dotPos)
    {
        if (dotPos.z >= -5 + this.transform.localScale.z &&
            dotPos.z <= 5 - this.transform.localScale.z) return -100;
        return Mathf.Abs(dotPos.z - (-5 + transform.localScale.z)) <=
            Mathf.Abs(5 - dotPos.z - transform.localScale.z) ? -5 + transform.localScale.z :
            5 - this.transform.localScale.z;
    }
    Vector3 moveDotOnEdge(Vector3 dotPos)
    {
        var newX = checkXAxis(dotPos);
        var newY = checkYAxis(dotPos);
        var newZ = checkZAxis(dotPos);
        return new Vector3(newX == -100 ? dotPos.x : newX,
            newY == -100 ? dotPos.y : newY,
            newZ == -100 ? dotPos.z : newZ);
    }
    public override void OnPointerUp(PointerEventData data)
    {
        if (this.name == "1") return;
        float dotDistance = this.name != "2" ? 2f : 3.5f;
        var dotPos = new Vector3(data.position.x, data.position.y, dotDistance);
        var transformedPos = Camera.main.ScreenToWorldPoint(dotPos);
        //if (dotInCube(transformedPos))
        //    transform.position = transformedPos;
        var movedDot = moveDotOnEdge(transformedPos);
        if (this.name == "2" && (movedDot.y < 1 || movedDot.y > 4 || movedDot.x < -4 || movedDot.x > 4 ||
            movedDot.z < -4 || movedDot.z > 4)) return;
        transform.position = movedDot;
    }

    //public override void OnScroll(PointerEventData data)
    //{
    //    Debug.Log("OnScroll called.");
    //}

    //public override void OnSelect(BaseEventData data)
    //{
    //    Debug.Log("OnSelect called.");
    //}

    //public override void OnSubmit(BaseEventData data)
    //{
    //    Debug.Log("OnSubmit called.");
    //}

    //public override void OnUpdateSelected(BaseEventData data)
    //{
    //    Debug.Log("OnUpdateSelected called.");
    //}

    // Update is called once per frame
    void Update ()
    {

    }
}
