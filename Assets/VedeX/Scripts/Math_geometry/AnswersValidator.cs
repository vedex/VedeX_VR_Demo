﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswersValidator : MonoBehaviour
{

    private const int PRISM = 0;
    private const int PYRAMID = 1;

    public Text q1,
            win,
            lose;

    public Button t1, t2, t3;
    private int figureType;
    private int ans = 0;
    string[] questions =
    {
        "Данная призма так же является:", "Данная пирамида так же является:"
    };
    string[] t1Text =
{
        "Параллелепипедом", "Тетраэдром"
    };
    string[] t2Text =
{
        "Наклонной", "Прямоугольной"
    };
    string[] t3Text =
{
        "Четырёхугольной", "Четырёхугольной"
    };

    // Use this for initialization
    void Start()
    {
        if (q1.text == "prism") figureType = PRISM;
        else if (q1.text == "pyramid") figureType = PYRAMID;
        t1.onClick.AddListener(delegate { onT1Changed(); });
        t2.onClick.AddListener(delegate { onT2Changed(); });
        t3.onClick.AddListener(delegate { onT3Changed(); });
        initQuestions();
    }

    void initQuestions()
    {
        q1.text = questions[figureType];
        t1.GetComponentsInChildren<Text>()[0].text = t1Text[figureType];
        t2.GetComponentsInChildren<Text>()[0].text = t2Text[figureType];
        t3.GetComponentsInChildren<Text>()[0].text = t3Text[figureType];
    }

    void onT1Changed()
    {
        if (figureType == PRISM)
        {
            t1.GetComponent<Image>().color = Color.green;
            ans++;
            validateAnswer();
        }
        else if (figureType == PYRAMID)
        {
            t1.GetComponent<Image>().color = Color.red;
            lose.gameObject.SetActive(true);
            hideAfterEnd();
        }
    }

    public void onT2Changed()
    {
        t2.GetComponent<Image>().color = Color.red;
        lose.gameObject.SetActive(true);
        hideAfterEnd();
    }

    public void onT3Changed()
    {
        t3.GetComponent<Image>().color = Color.green;
        ans++;
        validateAnswer();
    }

    void validateAnswer()
    {
        if (ans == 2 && figureType == PRISM)
        {
            win.gameObject.SetActive(true);
            hideAfterEnd();
        }
        else if (ans == 1 && figureType == PYRAMID)
        {
            win.gameObject.SetActive(true);
            hideAfterEnd();
        }
    }

    void hideAfterEnd()
    {
        t1.gameObject.SetActive(false);
        t2.gameObject.SetActive(false);
        t3.gameObject.SetActive(false);
        q1.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
