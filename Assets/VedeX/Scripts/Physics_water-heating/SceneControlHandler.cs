﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneControlHandler : MonoBehaviour {
	public GameObject text;
	public GameObject answers;

	public void PerformExit() {
		//TODO
	}

	public void PerformFinish() {
		Text msg = text.GetComponent<Text> ();
		msg.alignment = TextAnchor.UpperLeft;
		msg.text = "Как изменится скорость атомов газа при его нагревании?";
		answers.SetActive (true);
	}

}
