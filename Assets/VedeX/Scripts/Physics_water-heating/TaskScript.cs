﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TaskScript : MonoBehaviour { 

	public static bool MODE_HEAT = true;
	private float temperature = 10;
	//public Text temperatureDisplay;

	private static string MOLECULE_TAG = "Hydro";

	Rigidbody[] molecules_rbs = null;

	private System.Random rand = new System.Random();
	public TextMesh tempDisplay;
	// Use this for initialization
	void Start () {
		GameObject[] molecules = GameObject.FindGameObjectsWithTag (MOLECULE_TAG);
		molecules_rbs = new Rigidbody[molecules.Length];
		int id = 0;
		foreach (GameObject molecule in molecules)
			molecules_rbs [id++] = molecule.GetComponent<Rigidbody> ();
		InvokeRepeating("AddForces", 0, 0.1f);
	}

	// Update is called once per frame
	void Update () {
		if (WaterContainerHandler.POINTER_DOWN) {
			if (MODE_HEAT)
				temperature = Mathf.Min(100.0f, temperature + 1.0f);
			else
				temperature = Mathf.Max(0.0f, temperature - 1.0f);
			tempDisplay.text = temperature.ToString () + " °C";
			if (temperature == 100.0f)
				tempDisplay.color = Color.red;
			else if (temperature == 0.0f)
				tempDisplay.color = Color.blue;
			else
				tempDisplay.color = Color.white;
		}
	}

	void AddForces() {
		foreach (Rigidbody rb in molecules_rbs) {
			float mul = temperature * (float)Math.Sqrt (temperature) / 3 + 1;
			float speedX = rand.Next (-1, 1) * mul;
			float speedY = rand.Next (-1, 1) * mul;
			float speedZ = rand.Next (-1, 1) * mul;
			Vector3 force = new Vector3(speedX / 10, speedY / 10, speedZ / 10);
			rb.AddRelativeForce (force);
		}
	}
}
