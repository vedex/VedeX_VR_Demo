﻿using UnityEngine;
using UnityEngine.EventSystems;

public class WaterContainerHandler : EventTrigger
{
	public static bool POINTER_DOWN = false;

	public override void OnPointerDown(PointerEventData data)
	{
		POINTER_DOWN = true;
	}

	public override void OnPointerUp(PointerEventData data)
	{
		POINTER_DOWN = false;
	}
}