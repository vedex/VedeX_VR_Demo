﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeatButtonsController : MonoBehaviour {

	public Button heatButton;
	public Button coolButton;

	public void EnableHeating() {
		TaskScript.MODE_HEAT = true;
		enableButton (heatButton);
		disableButton (coolButton);
	}
	public void DisableHeating() {
		TaskScript.MODE_HEAT = false;
		enableButton (coolButton);
		disableButton (heatButton);
	}

	private void enableButton(Button btn) {
		ColorBlock cb = btn.colors;
		cb.normalColor = new Color (0.1f, 0.73f, 0.12f);
		btn.colors = cb;
	}
	private void disableButton(Button btn) {
		ColorBlock cb = btn.colors;
		cb.normalColor = new Color (0.6f, 0.6f, 0.6f);
		btn.colors = cb;
	}
}
