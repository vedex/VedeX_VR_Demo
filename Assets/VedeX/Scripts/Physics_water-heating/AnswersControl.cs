﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswersControl : MonoBehaviour {
	public Button correct;
	public Button incorrect_1;
	public Button incorrect_2;
	bool isAnswered = false;

	public void CorrectPressed() {
		if (isAnswered) return;
		enableButton (correct);
		disableHighlighting (correct);
		disableHighlighting (incorrect_1);
		disableHighlighting (incorrect_2);
		isAnswered = true;
	}

	public void Incorrect1Pressed() {
		if (isAnswered) return;
		enableButton (correct);
		disableButton (incorrect_1);
		disableHighlighting (correct);
		disableHighlighting (incorrect_1);
		disableHighlighting (incorrect_2);
		isAnswered = true;
	}

	public void Incorrect2Pressed() {
		if (isAnswered) return;
		enableButton (correct);
		disableButton (incorrect_2);
		disableHighlighting (correct);
		disableHighlighting (incorrect_1);
		disableHighlighting (incorrect_2);
		isAnswered = true;
	}

	private void enableButton(Button btn) {
		ColorBlock cb = btn.colors;
		cb.normalColor = new Color (0.1f, 0.73f, 0.12f);
		disableHighlighting (btn);
		btn.colors = cb;
	}
	private void disableButton(Button btn) {
		ColorBlock cb = btn.colors;
		cb.normalColor = new Color (1.0f, 0.0f, 0.0f);
		disableHighlighting (btn);
		btn.colors = cb;
	}

	private void disableHighlighting(Button btn) {
		ColorBlock cb = btn.colors;
		cb.highlightedColor = cb.normalColor;
		btn.colors = cb;
	}
}
