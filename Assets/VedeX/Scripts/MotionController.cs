﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionController : MonoBehaviour {

	public GameObject camera;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");
		Transform cameraRot = camera.GetComponent<Transform> ();
		float angle = cameraRot.eulerAngles.y * Mathf.PI / 180.0f;
		if (angle < 0)
			angle += 2 * Mathf.PI;
		float hn = h * Mathf.Cos (angle) + v * Mathf.Sin (angle);
		float vn = -h * Mathf.Sin (angle) + v * Mathf.Cos (angle);
		transform.position += new Vector3 (hn * 0.05f, 0, vn * 0.05f);
		transform.rotation.SetEulerAngles (new Vector3 (0, 0, 0));
		checkBorder ();
	}

	void checkBorder(){
		if(this.transform.position.x >= 4.5f)
			this.transform.position = new Vector3(4.5f, this.transform.position.y, this.transform.position.z);
		if(this.transform.position.x <= -4.5f)
			this.transform.position = new Vector3(-4.5f, this.transform.position.y, this.transform.position.z);
		if(this.transform.position.z <= -4.5f)
			this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -4.5f);
		if(this.transform.position.z >= 4.5f)
			this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 4.5f);
	}
}
